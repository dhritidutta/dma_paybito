import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdershortcutComponent } from './ordershortcut.component';

describe('OrdershortcutComponent', () => {
  let component: OrdershortcutComponent;
  let fixture: ComponentFixture<OrdershortcutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdershortcutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdershortcutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
