import { Component, OnInit, Inject, DoCheck, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { OrderbookComponent } from '../orderbook/orderbook.component';
import { MarketwatchComponent } from "../marketwatch/marketwatch.component";
import { ChartoneComponent } from "../chartone/chartone.component";
import { OrdershortcutComponent } from '../ordershortcut/ordershortcut.component'
import { TickerComponent } from "../ticker/ticker.component";
import { CharttwoComponent } from "../charttwo/charttwo.component";
import { DemosorComponent } from "../demosor/demosor.component";
import { TvChartContainerComponent } from "../tv-chart-container/tv-chart-container.component";
import { AnnouncementComponent } from "../announcement/announcement.component";
import * as $ from "jquery";
import { from } from 'rxjs';
import { FormControl, Validators } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { BLACK_ON_WHITE_CSS_CLASS } from '@angular/cdk/a11y/high-contrast-mode/high-contrast-mode-detector';
import { CoreDataService } from '../core-data.service';
import { MarketdataModule } from '../marketdata/marketdata.module';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { BinanceService } from '../services/binance.service';
import {
  NgbModal
} from '@ng-bootstrap/ng-bootstrap';
export interface DialogData {
  animal: string;
  name: string;
}
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})

export class NavbarComponent implements OnInit {
  animal: string;
  name: string;
  loaded = false;
  myComponent?: any;
  username: any;
  shorcutdatafinal: any;
  exchange: any;
  apikey:any;
  secretkey:any
  // Email:string;
  // public Password:any;

  // demo_html = require('!!html-loader!./navbar.component.html');
  // demo_ts = require('!!raw-loader!./navbar.component.ts');
  // demo_scss = require('!!raw-loader!./navbar.component.css');


  constructor(private _BinanceService: BinanceService, public modalService: NgbModal, private viewContainerRef: ViewContainerRef, private cfr: ComponentFactoryResolver, public data: CoreDataService, public _TvChartContainerComponent: TvChartContainerComponent, public _OrdershortcutComponent: OrdershortcutComponent, public dialog: MatDialog, public _OrderbookComponent: OrderbookComponent, public _MarketwatchComponent: MarketwatchComponent, public _ChartoneComponent: ChartoneComponent, public _CharttwoComponent: CharttwoComponent, public _DemosorComponent: DemosorComponent, public _TickerComponent: TickerComponent, public _AnnouncementComponent: AnnouncementComponent) { }

  ngOnInit(): void {

    this.username = localStorage.getItem('username');
    //  alert(this.username);
    // this.animate()
    //  this._TickerComponent.getdata();
    this.shorcutdatafinal = this._TickerComponent.shortcutdata;
    console.log('+++++++++tttttttttttttttddddddddddd', this.shorcutdatafinal);
   
    this.apikey=localStorage.getItem('apikey');
    this.secretkey=localStorage.getItem('secretkey');
   

  }

  async load() {
    this.viewContainerRef.clear();
    const { TickerComponent } = await import('../ticker/ticker.component');
    this.viewContainerRef.createComponent(
      this.cfr.resolveComponentFactory(TickerComponent)
    );

  }
  animate() {

    var block_text = $('.ticker li').map(function () { return $(this).html(); }).toArray();
    $(".ticker").html("<p>" + block_text + "</p>");
    var ticker_text = $('.ticker p');
    var ticker_width = $(".ticker").width();
    var text_x = ticker_width;

    var scroll_ticker = function () {
      text_x--;
      ticker_text.css("left", text_x);
      if (text_x < -1 * ticker_text.width()) {
        text_x = ticker_width;
      }
    }

    setInterval(scroll_ticker, 10);


  }
  openDialog(): void {
   
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      // width: '535px',
      // height:'500px',
     
      data: { apikey: this.name, animal: this.animal }
    });

    dialogRef.afterClosed().subscribe(result => {

      console.log('The dialog was closed');
      this.animal = result;
    });
  }
  logout() {
    // alert('a');
    // document.getElementById('log-view').style.visibility='block';
    // document.getElementById('logout-view').style.visibility='none';
    var action = "delete"
    this._BinanceService.logout(action)
      .subscribe(data => {
      console.log('===========',data);


      var response=data;

      // if (response='key deleted successfully'){

      // }
      this.apikey="";
      this.secretkey="";
      localStorage.clear();
    this.data.alert('Logout Successful!', 'danger');
    localStorage.clear();
      })
    
  }
  ngDoCheck() {
    this.username = localStorage.getItem('username');
   // alert();
  }
  // Opendatawindow(){


  // }

  keymodal(key,event) {

    //  alert('ddddddddd');
      
    this.apikey=localStorage.getItem('apikey');
  
    if(event=='apikey'){
      this.modalService.open(key, {
        centered: true
      });
    }
   
  }
  getexchange(event) {
    // alert(event);
    this.exchange = event;
  }
  saveKey() {
    // debugger;
    var apikey = (<HTMLInputElement>document.getElementById('apikey')).value;
    var secretkey = (<HTMLInputElement>document.getElementById('secretkey')).value;
    var userId=localStorage.getItem('userID');
//alert(userId);
    if(this.exchange==undefined){
      this.exchange='binance';
    }
    if(userId!=null){
      if(apikey!=""||secretkey!=""){
      this._BinanceService.saveAPIkey(userId, this.exchange, apikey, secretkey)
      .subscribe(data => {
         console.log('mmmmmmmmmmmmm',data);
         if(data[0].return_id=='1'){
          this.data.alert('Key inserted sucessfully!', 'success');
         }
         else{
       // var error=data[0].msg;
       // alert(data[0].msg)
         this.data.alert("Please Login to save api key!" ,'danger');
         }
      
      })
    }
    else{
      this.data.alert("Apikey or secretkey missing!" ,'danger');
    }
    }
    else{
      this.data.alert('Please login to save API key!', 'danger');
    }
   
    //     let data = 
    //     '\r Name: ' + name + ' \r\n ' + 
    //     'Age: ' +age;


    // // Convert the text to BLOB.
    // const textToBLOB = new Blob([data], { type: 'text/plain' });
    // const sFileName = 'formData.txt';	   // The file to save the data.

    // let newLink = document.createElement("a");
    // newLink.download = sFileName;

    // if (window.webkitURL != null) {
    //     newLink.href = window.webkitURL.createObjectURL(textToBLOB);
    // }
    // else {
    //     newLink.href = window.URL.createObjectURL(textToBLOB);
    //     newLink.style.display = "none";
    //     document.body.appendChild(newLink);
    // }

    // newLink.click(); 

  }

  Openwatchlist() {
    $("#daataTab").style.display = 'block';
  }

}
@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'login-dialog.html',
})
export class DialogOverviewExampleDialog {

  public email: any;
  //  public Pass:any;
  public password: any;
  apikey;any;
  secretkey:any;
  constructor(public _BinanceService: BinanceService,
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, public _CoreDataService: CoreDataService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
  login() {
    this._CoreDataService.alert("Loading...", "dark");
    this._CoreDataService.loader = true;
    var loginObj = {};
    loginObj['email'] = this.email;
    loginObj['password'] = this.password;
    loginObj['exchange'] = 'binance';
    // var email = this.email;
    // var password = this.password;
    // var password = this.password;
    // var exchange = 'binance';
    this._BinanceService.login(loginObj)
      .subscribe(data => {
        var result =data;
     
        var userID = result[0].user_id;
        var apikey=result[0].api_key;
       // console.log('login------------------', userID +apikey);
    //  alert(userID+""+apikey+"");
       localStorage.setItem('apikey',apikey);
     //  localStorage.setItem('secretkey',secretkey);
        localStorage.setItem('userID', userID);
        localStorage.setItem('username', 'User Name');
        var username = localStorage.getItem('username');
        
       // alert(username);
       this._CoreDataService.alert('Login Successful!', 'success');
        this.dialogRef.close();
      })
  }
  // login() {
  //   this._CoreDataService.alert("Loading...", "dark");
  //   this._CoreDataService.loader=true;
  //   var loginObj = {};
  //   loginObj['email'] = this.email;
  //   loginObj['password'] = this.password;

  //   // loginObj['email']='broker-admin@hashcash.com';
  //   // loginObj['password']='Abcd@123';

  //   this._CoreDataService.loginthrouIdpassword(loginObj)
  //     .subscribe(response => {

  //       if (response.error.error_data == 0) {
  //         var userdetails = response.user;

  //         //   email: "broker-admin@hashcash.com"
  //         // fullName: "brokerAdmin"
  //         // password: ""
  //         // phone: "1111111100"
  //         // userStatus: 1
  //         // user_id: 1
  //         var Ustatus = response.user['userStatus'];
  //         var Uname = response.user['fullName'];
  //         var Userid = response.user['userId'];
  //          console.log('iiiiii',Userid);
  //         localStorage.setItem('userstatus', Ustatus);
  //         localStorage.setItem('username', Uname);
  //         localStorage.setItem('userId', Userid);

  //         // console.log('iiiiii',userdetails);
  //         this._CoreDataService.loader=false;
  //         this._CoreDataService.alert('Login Successful!', 'danger');
  //         // document.getElementById('log-view').style.visibility='none';
  //         // document.getElementById('logout-view').style.visibility='block';
  //         this.dialogRef.close();
  //       }
  //       else {

  //         var errormessage = response.error.error_msg;
  //         this._CoreDataService.alert(errormessage, 'danger');
  //       }



  //     })
  // }

}