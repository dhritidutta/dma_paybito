import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import{NavbarComponent} from "../app/navbar/navbar.component";
import{OrderbookComponent} from "../app/orderbook/orderbook.component";
import{LayoutComponent} from "../app/layout/layout.component";
import{LoginComponent} from "../app/login/login.component";
import{TickerComponent} from "../app/ticker/ticker.component";
import{TvChartContainerComponent} from "../app/tv-chart-container/tv-chart-container.component";
import{CharttwoComponent} from "../app/charttwo/charttwo.component";
const routes: Routes = [
  { path: 'test', component: NavbarComponent },
  { path: 'orderbook', component: OrderbookComponent },
  { path: '', component: LayoutComponent },
  { path: 'login', component: LoginComponent },
  { path: 'ticker', component: TickerComponent },
  { path: 'chart', component: TvChartContainerComponent },
  { path: 'charttwo', component: CharttwoComponent },
  { path: 'marketdata', loadChildren: () => import('./marketdata/marketdata.module').then(m => m.MarketdataModule) }
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
