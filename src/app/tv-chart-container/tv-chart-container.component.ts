import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import * as io from 'socket.io-client';
//const socket = io('http://localhost:3000');
import {
    widget,
    IChartingLibraryWidget,
    ChartingLibraryWidgetOptions,
    LanguageCode,
} from '../../assets/charting_library/charting_library.min';
declare var $: any;

@Component({
    selector: 'app-tv-chart-container',
    templateUrl: './tv-chart-container.component.html',
    styleUrls: ['./tv-chart-container.component.css']
})
export class TvChartContainerComponent implements OnInit, OnDestroy {
   // private _symbol: ChartingLibraryWidgetOptions['symbol'] = localStorage.getItem("buying_crypto_asset")+'/'+localStorage.getItem("selling_crypto_asset");
    private _symbol: ChartingLibraryWidgetOptions['symbol'] ='XRP'+'/'+'USDT';
    private _interval: ChartingLibraryWidgetOptions['interval'] = '1D';
    //private _datafeedUrl = 'https://api.paybito.com:8443/api/public';
    private _datafeedUrl = 'http://54.219.91.71:3100/public';
    private _libraryPath: ChartingLibraryWidgetOptions['library_path'] = '/assets/charting_library/';
    private _chartsStorageUrl: ChartingLibraryWidgetOptions['charts_storage_url'] = 'https://saveload.tradingview.com';
    private _chartsStorageApiVersion: ChartingLibraryWidgetOptions['charts_storage_api_version'] = '1.1';
    private _clientId: ChartingLibraryWidgetOptions['client_id'] = 'tradingview.com';
    private _userId: ChartingLibraryWidgetOptions['user_id'] = 'public_user_id';
    private _fullscreen: ChartingLibraryWidgetOptions['fullscreen'] = false;
    private _autosize: ChartingLibraryWidgetOptions['autosize'] = true;
    private _containerId: ChartingLibraryWidgetOptions['container_id'] = 'tv_chart_container';
    private _tvWidget: IChartingLibraryWidget | null = null;
    private buyingAsset: any;
    private sellingAsset: any;



    @Input()
    set symbol(symbol: ChartingLibraryWidgetOptions['symbol']) {
        this._symbol = symbol || this._symbol;
    }

    @Input()
    set interval(interval: ChartingLibraryWidgetOptions['interval']) {
        this._interval = interval || this._interval;
    }

    @Input()
    set datafeedUrl(datafeedUrl: string) {
        this._datafeedUrl = datafeedUrl || this._datafeedUrl;
    }

    @Input()
    set libraryPath(libraryPath: ChartingLibraryWidgetOptions['library_path']) {
        this._libraryPath = libraryPath || this._libraryPath;
    }

    @Input()
    set chartsStorageUrl(chartsStorageUrl: ChartingLibraryWidgetOptions['charts_storage_url']) {
        this._chartsStorageUrl = chartsStorageUrl || this._chartsStorageUrl;
    }

    @Input()
    set chartsStorageApiVersion(chartsStorageApiVersion: ChartingLibraryWidgetOptions['charts_storage_api_version']) {
        this._chartsStorageApiVersion = chartsStorageApiVersion || this._chartsStorageApiVersion;
    }

    @Input()
    set clientId(clientId: ChartingLibraryWidgetOptions['client_id']) {
        this._clientId = clientId || this._clientId;
    }

    @Input()
    set userId(userId: ChartingLibraryWidgetOptions['user_id']) {
        this._userId = userId || this._userId;
    }

    @Input()
    set fullscreen(fullscreen: ChartingLibraryWidgetOptions['fullscreen']) {
        this._fullscreen = fullscreen || this._fullscreen;
    }

    @Input()
    set autosize(autosize: ChartingLibraryWidgetOptions['autosize']) {
        this._autosize = autosize || this._autosize;
    }

    @Input()
    set containerId(containerId: ChartingLibraryWidgetOptions['container_id']) {
        this._containerId = containerId || this._containerId;
    }

    ngOnInit() {
        this.buyingAsset = 'XRP';

        this.sellingAsset = 'USDT';
        this._symbol = (this.buyingAsset) + '/' + this.sellingAsset;
        // alert(this._symbol);
        var test = localStorage.getItem('themecolor');
        // socket.on('data2', (res) => {
        //     var data = res.msg;
        //     console.log('data2',data);

        // })
        // function getLanguageFromURL(): LanguageCode | null {
        //     const regex = new RegExp('[\\?&]lang=([^&#]*)');
        //     const results = regex.exec(location.search);

        //     return results === null ? null : decodeURIComponent(results[1].replace(/\+/g, ' ')) as LanguageCode;
        // }

        const widgetOptions: ChartingLibraryWidgetOptions = {
            // debug:true,
            symbol: this._symbol,
            datafeed: new (window as any).Datafeeds.UDFCompatibleDatafeed(this._datafeedUrl),
            interval: this._interval,
            container_id: this._containerId,
            library_path: this._libraryPath,
            locale: 'en',
            disabled_features: ['use_localstorage_for_settings', 'header_saveload', 'header_settings', 'header_compare', 'header_symbol_search', 'header_chart_type'],
            enabled_features: ['hide_left_toolbar_by_default', 'header_indicators'],
            charts_storage_url: this._chartsStorageUrl,
            charts_storage_api_version: this._chartsStorageApiVersion,
            client_id: this._clientId,
            user_id: this._userId,
            fullscreen: this._fullscreen,
            autosize: this._autosize,
            theme: "Dark"


        };

        const tvWidget = new widget(widgetOptions);
        this._tvWidget = tvWidget;

        // tvWidget.onChartReady(() => {
        //     tvWidget.headerReady().then(() => {
        //         const button = tvWidget.createButton();
        //     });
        // });
        tvWidget.onChartReady(() => {
            tvWidget.headerReady().then(() => {
                const button = tvWidget.createButton();
                button.setAttribute('title', 'Click to show a notification popup');
                button.classList.add('apply-common-tooltip');
                button.addEventListener('click', () => tvWidget.showNoticeDialog({
                    title: 'Notification',
                    body: 'TradingView Charting Library API works correctly',
                    callback: () => {
                        console.log('Noticed!');
                    },
                }));
                button.innerHTML = 'Check API';
            });
        });

    }
    // getParameterByName(theme){
    //     var name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    // 			var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    // 				results = regex.exec(location.search);
    // return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));


    // }
    //     ngDoCheck(){
    //         this.buyingAsset=localStorage.getItem("buying_crypto_asset").toLocaleUpperCase();
    //         this.sellingAsset=localStorage.getItem("selling_crypto_asset").toLocaleUpperCase();
    //         if(localStorage.getItem("buying_crypto_asset") !='BTC'){
    //         this._tvWidget.chart().setSymbol(this._symbol,()=>{

    //         });
    //     }
    //     this.changeTheme();
    // }

    //     changeTheme(){
    //         this._tvWidget.onChartReady(() => {
    //            this._symbol=(this.buyingAsset) +'/'+this.sellingAsset;
    //             var test=localStorage.getItem('themecolor').toString();
    //              if(test=="Light"){
    //              this._tvWidget.changeTheme('Light');
    //            }
    //            else {
    //              this._tvWidget.changeTheme('Dark');
    //            }
    //        });
    //    }

    closetvchart() {
        document.getElementById("tvchart").style.display = 'none';
    }
    opentvchart() {
        document.getElementById("tvchart").style.display = 'block';
    }
    restorewindow(){
       // alert('restorewindow');
        $("#paneltv").slideToggle();
        $('.tvchart-tab').removeClass('but-plus'); 
        document.getElementById("tvchart").style.display = 'none';
       
        
        document.getElementById("restore11").style.display = 'none'; 
        document.getElementById("btnclosetv").style.display = 'block'; 
        document.getElementById("minimizetv").style.display = 'block'; 
        document.getElementById("tvchart").style.display = 'block';
        $(".tvchart-tab").animate({
            
            height: '50%',
            width: '100%',
         });
    }
   
    ngOnDestroy() {
        if (this._tvWidget !== null) {
            this._tvWidget.remove();
            this._tvWidget = null;
        }
    }
    buttonclick(){
       
        // $('.mintv').toggleClass('btn-plus');
        // document.getElementById("ordershortcut").style.display = 'none';
        // document.getElementById("tickerview").style.display = 'none';
        // document.getElementById("demosor").style.display = 'none';
        // document.getElementById("marketwatch").style.display='none'; 
        $("#paneltv").slideToggle();
        document.getElementById("restore11").style.display = 'block'; 
        document.getElementById("btnclosetv").style.display = 'none'; 
        document.getElementById("minimizetv").style.display = 'none'; 
        $(".tvchart-tab").animate({
           
            height: '10%',
            width: '24%',
         });
         $('.tvchart-tab').toggleClass('but-plus'); 
         
     }
}