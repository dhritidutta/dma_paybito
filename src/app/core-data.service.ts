import { Injectable } from '@angular/core';
import * as $ from 'jquery';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CoreDataService {
  reason: any;
  icon: any;
  loader:boolean=false;
  SelectedBuyasset:any;
  SelectedSellasset:any;
  Lasttraderprice:any;
  source:any;
  courceConn:any;
  source2:any;
 sourse3:any;
 symbol;any;
 tempsearch:any;
  baseURL='http://184.72.11.42:7503/';
  constructor(private http: HttpClient) {
    var alertPl = `<div class="alertPlace"></div>`;
    $('html').append(alertPl).fadeIn();
   }


  alert(msg, type, time = 15000) {
    //console.log(msg, type);
    
    this.reason = msg;
    this.icon = 'puff';

    if (msg == 'Loading...') {
      this.loader = true;
      setTimeout(() => {
        this.loader = false;
      }, 10000);
    } else {
      $('.alert:first').fadeOut();
      var htx = `<div class="alert alert-` + type + ` my-2" role="alert">` + msg + `</div>`;
      $('.alertPlace').append(htx).fadeIn();
     // console.log('/////////////////////////////',htx);
      setTimeout(() => {
        $('.alert:last').remove().fadeOut();
      }, time);
    }
  }
  loginthrouIdpassword(data){
    return this.http.post<any>(this.baseURL+'BrokerApp/user/login',JSON.stringify(data),{ headers: { 'content-Type': 'application/json' } }); 
  }
  //http://184.72.11.42:7503/BrokerApp/user/login
}
