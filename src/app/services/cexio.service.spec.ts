import { TestBed } from '@angular/core/testing';

import { CexioService } from './cexio.service';

describe('CexioService', () => {
  let service: CexioService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CexioService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
