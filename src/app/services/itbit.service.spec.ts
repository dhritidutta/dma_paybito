import { TestBed } from '@angular/core/testing';

import { ItbitService } from './itbit.service';

describe('ItbitService', () => {
  let service: ItbitService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ItbitService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
