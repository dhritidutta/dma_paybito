import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
//const socket = io('http://54.219.91.71:3100');
import { CoreDataService } from '../core-data.service';
const socket = io('http://localhost:3100');
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { sha256, sha224 } from 'js-sha256';
@Injectable({
  providedIn: 'root'
})
export class CoinbaseproService {

  constructor(private http: HttpClient, public data: CoreDataService) { }


  Getallassets() {
    // return this.http.get<any>('http://localhost:3400/public/assets')
    return this.http.get<any>('https://api.pro.coinbase.com/products');

  }
  getsnapshot(symbol) {
    /// debugger;
   return this.http.get<any>('http://localhost:3400/public/orderbooksnapshot?symbol='+symbol);
      
  }
}
