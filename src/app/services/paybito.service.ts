import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PaybitoService {
  public baseURL="https://api.paybito.com/api";
  public sourcebalance:any;
  constructor(private http: HttpClient) { }
  Getaccesstoken(body){
    let options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded').set('authorization', 'Basic cGF5Yml0by13ZWItY2xpZW50OlB5Z2h0bzM0TEpEbg==')
    };
    return this.http.post<any>(this.baseURL+ '/oauth/token', body.toString(), options)
     
  }
  GetUserbalance(jsonString){
    return this.http.post<any>(this.baseURL + '/transaction/getUserBalance', JSON.stringify(jsonString), {
      headers: {
        'Content-Type': 'application/json',
        'authorization': 'BEARER ' + localStorage.getItem('access_token')
      }
    })
  }
}
