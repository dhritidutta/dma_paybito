import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
//const socket = io('http://54.219.91.71:3100');
import { CoreDataService } from '../core-data.service';
const socket = io('http://localhost:3100');
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { sha256, sha224 } from 'js-sha256';

@Injectable({
  providedIn: 'root'
})
export class BinanceService {
  public orderbook;
  public alltickers = [];
  public shortcutasset=[];
  public shortcutsymbol=[]
  stremaskdata = [];
  strembiddata = [];
  askdata;
  biddata;
  orderdata: any;
  lastupdateId: any;
  ticker;
  askpricelowest;
  bidpricehighest;
  askamtlowest;
  bidamthigest;
  constructor(private http: HttpClient, public data: CoreDataService) { }


  gettickerdata() {
    //alert('aaaaaaaaaa');
    // socket.on('datak', (res) => {
    //   var test1 = res.msg;
    //   console.log('7777777777777777', test1)

    // })

    socket.on('allticker', (res) => {
      this.alltickers = res.msg;
      
      console.log('99999999999', this.alltickers)
     
    //  alert(this.shortcutasset);

      if(this.data.tempsearch==undefined && this.shortcutasset.length==0){
     //   alert(this.data.tempsearch);
        for (var i=0; i <=9; i++) {
          this.shortcutasset.push({'symbol':this.alltickers[i].symbol,'currentClose':this.alltickers[i].currentClose});
        }

      }
     else{
      for (var i = 0; i < this.alltickers.length; i++) {

        if(this.alltickers[i].symbol==this.data.tempsearch && this.shortcutasset.length<=10){
        const addedasset = this.shortcutsymbol;
        var found = addedasset.includes(this.data.tempsearch);

        if (found == false && this.shortcutasset.length==10) {
         // this.shortcutasset.pop();
         this.shortcutasset.splice(0,1,{'symbol':this.alltickers[i].symbol,'currentClose':this.alltickers[i].currentClose})
         // this.shortcutasset.push({'symbol':this.alltickers[i].symbol,'currentClose':this.alltickers[i].currentClose});
          this.shortcutsymbol.push(this.alltickers[i].symbol);
        }
      }
    }
   
   } 
   console.log('00000000000000000000000',this.shortcutasset); 
    })


  }
  getOnertickerdata() {

    socket.on('ticker', (res) => {
      this.ticker = res.msg;
      console.log('10101010101010', this.ticker)

    })
  }
  SendSymboltoServer(symbol) {
    //  alert(symbol);
    var Symbol = symbol;
     socket.emit('join',symbol);
    // socket.on('connect', function () {
    //   socket.emit('join', symbol);

    // });
  }
  discunnectSocket() {
    socket.on('ticker', (res) => {
      socket.close();
      console.log('10close close', this.ticker)
    });
  }
  Getorderbookdata() {
    // socket.on('Ordersnapshot', (res) => {
    //   this.orderbook = res.msg;

    //   //return this.orderbook;
    //   if(this.orderbook!=null){
    //     this.askdata=this.orderbook.asks;
    //     this.biddata=this.orderbook.bids;
    //   }
    //  console.log('snapshot1',  this.askdata);
    // // console.log('snapshot2',  this.biddata);

    // })
    socket.on('Order', (res) => {
      //debugger;
      this.orderbook = res.msg;
      this.orderdata = this.orderbook;
      this.stremaskdata = this.orderbook.askDepthDelta;
      this.strembiddata = this.orderbook.bidDepthDelta;

      var streamask = this.stremaskdata;
      var streamdiddata = this.strembiddata;
      //var lastupdateId = data.lastUpdateId;
      //return this.orderbook;

      if (this.orderdata.lastUpdateId <= this.lastupdateId) {
        if (this.orderdata.firstUpdateId = lastu + 1) {
          if (this.orderdata.firstUpdateId <= this.lastupdateId + 1 && this.orderdata.lastUpdateId >= this.lastupdateId + 1) {
            // alert('hgjhfjhf');
            // this.askdata.push({})
            for (var i = 0; i < this.askdata.length; i++) {
              var price = this.askdata[i][0];
              var quantity = this.askdata[i][1];
              for (var x = 0; x < streamask.length; x++) {
                if (streamask[i].price = price) {
                  this.askdata[i][1] = streamask[x].quantity;
                }
                else {
                  var price = streamask[x].price;
                  var quantity = streamask[x].quantity;
                  this.askdata[i].push({ price, quantity });
                }
              }

            }
            for (var i = 0; i < this.biddata.length; i++) {
              var price = this.biddata[i][0];
              var quantity = this.biddata[i][1];
              for (var x = 0; x < streamdiddata.length; x++) {
                if (streamdiddata[i].price = price) {
                  this.biddata[i][1] = streamdiddata[x].quantity;
                }
                else {
                  var price = streamdiddata[x].price;
                  var quantity = streamdiddata[x].quantity;
                  this.biddata[i].push({ price, quantity });
                }
              }

            }
          }
          console.log('8888888888888', this.orderbook);
          var lastu = this.orderdata.lastUpdateId;
        }
      }
    })

  }
  Getorderbookdepth() {
    socket.on('Orderd', (res) => {

      //return this.orderbook;
      // console.log('777777777777777', res.msg)

    })

  }
  getsnapshot(symbol) {
    /// debugger;
    this.http.get<any>('https://www.binance.com/api/v1/depth?symbol=' + symbol + '&limit=1000')
      .subscribe(data => {
        var orserbookstreamdata = data;
        this.askdata = data.asks;
        this.biddata = data.bids;
        this.orderdata = this.orderbook;
        console.log('snapshot---------', this.orderdata);
        var streamask = this.stremaskdata;
        var streamdiddata = this.strembiddata;
        this.lastupdateId = data.lastUpdateId;
        this.askpricelowest = this.askdata[0][0];
        this.askamtlowest = this.askdata[0][1];
        this.bidpricehighest = this.biddata[0][0];
        this.bidamthigest = this.biddata[0][1];

      })
  }

  allOrders(symbol) {
    //     var today = new Date();
    //     var yyyy = today.getTime();
    //     var todaytime = Math.floor(yyyy / 1000);

    //  var testcode= sha256.hmac('nSwnLuZRC3wSD72L4i6kPctCVHPYUXNoFUnZ6YhpX04gNqmmXHDVKIYz9B2Zb6Hi', 'symbol=BTCUSDT&timestamp='+todaytime);


    return this.http.get<any>('http://localhost:3100/public/allorders?symbol=' + symbol)
  }
  AccountInfo() {
    return this.http.get<any>('http://localhost:3100/public/accountInfo')
  }
  Getallassets() {
    return this.http.get<any>('https://api.binance.com/api/v3/exchangeInfo')
  }
  Sellorder(symbol, side, type, timeInForce, quantity, price) {

    if (type == 'LIMIT') {
      return this.http.get<any>('http://localhost:3100/public/placeorder?symbol=' + symbol + '&side=' + side + '&type=' + type + '&timeInForce=' + timeInForce + '&quantity=' + quantity + '&price=' + price)
    }
    else if (type == 'MARKET') {
      return this.http.get<any>('http:/localhost:3100/public/placeorder?symbol=' + symbol + '&side=' + side + '&type=' + type + '&quantity=' + quantity)
    }

  }
  saveAPIkey(userId, exchange, apikey, secretkey) {
    return this.http.get<any>('http://localhost:3100/public/getAPikey?userid=' + userId + '&exchange=' + exchange + '&apikey=' + apikey + '&secretkey=' + secretkey);
  }
  getdipositaddress(asset) {
    return this.http.get<any>('http://localhost:3100/public/deposit?asset=' + asset)
  }
  submitwithdowrequest(asset, address, amount) {
    return this.http.get<any>('http://localhost:3100/public/withdraw?asset=' + asset + '&address=' + address + '&amount=' + amount)
  }
  login(data) {
    // return this.http.get<any>('http://localhost:3100/public/login?username='+username+'&password='+password+'&exchange='+exchange)
    return this.http.post<any>('http://localhost:3100/public/login', JSON.stringify(data), { headers: { 'content-Type': 'application/json' } });
  }
  logout(action) {
    return this.http.get<any>('http://localhost:3100/public/deletekey?action=' + action)
  }
}
