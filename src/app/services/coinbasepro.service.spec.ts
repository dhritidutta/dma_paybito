import { TestBed } from '@angular/core/testing';

import { CoinbaseproService } from './coinbasepro.service';

describe('CoinbaseproService', () => {
  let service: CoinbaseproService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CoinbaseproService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
