import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularDraggableModule } from 'angular2-draggable';
import { FormsModule }   from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { NavbarComponent } from './navbar/navbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon'; 
import {MatMenuModule} from '@angular/material/menu';
import { LayoutComponent } from './layout/layout.component';
import { OrderbookComponent } from './orderbook/orderbook.component';
import { TickerComponent } from './ticker/ticker.component';
import { WatchlistComponent } from './watchlist/watchlist.component';
import {MatButtonModule} from '@angular/material/button';
import { MarketwatchComponent } from './marketwatch/marketwatch.component';
import { ChartoneComponent } from './chartone/chartone.component';
import { CharttwoComponent } from './charttwo/charttwo.component';
import{DialogOverviewExampleDialog} from '../app/navbar/navbar.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSelectModule} from '@angular/material/select';
import { ReactiveFormsModule } from '@angular/forms';
import {MatExpansionModule} from '@angular/material/expansion';

import {MatTabsModule} from '@angular/material/tabs';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { OrdershortcutComponent } from './ordershortcut/ordershortcut.component';
import { DemosorComponent } from './demosor/demosor.component';
// import { NgbdSortableHeader } from './demosor/demosor.component';

import { TvChartContainerComponent } from './tv-chart-container/tv-chart-container.component';
import{CoreDataService} from '../app/core-data.service';
import { HttpClientModule } from "@angular/common/http";
import { AnnouncementComponent } from './announcement/announcement.component';
import { LoaderComponent } from './loader/loader.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    LayoutComponent,
    OrderbookComponent,
    WatchlistComponent,
    MarketwatchComponent,
    ChartoneComponent,
    CharttwoComponent,
    DialogOverviewExampleDialog,
    TickerComponent,
    OrdershortcutComponent,
    DemosorComponent,
  
    TvChartContainerComponent,
    AnnouncementComponent,
    LoaderComponent
 
   
  ],
  imports: [MatExpansionModule,ReactiveFormsModule ,MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,MatToolbarModule,MatIconModule,MatMenuModule,MatButtonModule,AngularDraggableModule,FormsModule,MatDialogModule,MatTabsModule, NgbModule
  ],
  exports: [MatExpansionModule,MatSelectModule,MatFormFieldModule,MatToolbarModule,MatIconModule,MatToolbarModule,MatMenuModule,MatButtonModule,MatDialogModule,MatTabsModule],
  entryComponents:[DialogOverviewExampleDialog],
  providers: [OrderbookComponent,MarketwatchComponent,ChartoneComponent,CharttwoComponent,TickerComponent,OrdershortcutComponent,DemosorComponent,TvChartContainerComponent,CoreDataService,AnnouncementComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
