import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemosorComponent } from './demosor.component';

describe('DemosorComponent', () => {
  let component: DemosorComponent;
  let fixture: ComponentFixture<DemosorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemosorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemosorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
