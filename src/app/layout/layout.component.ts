import { Component, OnInit } from '@angular/core';
import{CoreDataService} from '../core-data.service';
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  constructor(public data:CoreDataService) { }

  ngOnInit(): void {
  }

}
